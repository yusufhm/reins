[![](https://images.microbadger.com/badges/image/yusufhm/alpine-node-docker.svg)](https://microbadger.com/images/yusufhm/alpine-node-docker "Get your own image badge on microbadger.com")

## Alpine Node with Docker
Installed from a node:8-alpine base, with docker.

### Trying it out:
Build it:
```sh
docker build -t alpine-node-docker .
```

Run it:
```sh
docker run -it --rm \
  --env HEROKU_LOGIN={heroku_login} \
  --env HEROKU_API_KEY={heroku_api_key} \
  --volume `pwd`:/app \
  --name "reins-builder" \
  -w /app alpine-node-docker \
  ash
```
