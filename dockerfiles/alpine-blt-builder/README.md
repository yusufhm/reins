## Alpine BLT builder
Installed from an alpine base, with composer.

To build, run the following:
```
docker build -t alpine-blt-builder .
```

### Trying it out:
With AWS credentials:
```
docker run -e AWS_ACCESS_KEY_ID={{your_aws_access_key}} -e AWS_SECRET_ACCESS_KEY={{your_aws_secret_access_key}} -it alpine-blt-builder /bin/ash
git clone https://user@bitbucket.org/owner/project.git
cd {project_dir}
blt deploy:build
```

Without AWS credentials:
```
docker run -it alpine-blt-builder /bin/ash
git clone https://user@bitbucket.org/owner/project.git
cd {project_dir}
blt deploy:build
```
