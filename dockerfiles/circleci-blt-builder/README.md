## CircleCI BLT builder
Installed from an CircleCI imaeg base, with prestissimo, gulp & grunt.

To build, run the following:
```
docker build -t circleci-blt-builder .
```

Without AWS credentials:
```
docker run -it circleci-blt-builder /bin/bash
git clone https://user@bitbucket.org/owner/project.git
cd {project_dir}
blt deploy:build
```
