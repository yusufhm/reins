## Alpine Apache with mod_php
Installed from an alpine base.

To build, run the following:
```
docker build -t alpine-apache-php .
```

### Testing it out
With environment variables:
```
docker run \
  --name project_name -d -p 9080:80 \
  -e AWS_ACCESS_KEY_ID={your_aws_access_key} \
  -e AWS_SECRET_ACCESS_KEY={your_aws_secret_access_key} \
  -e DEPLOY_FILE=s3://{BUCKET.URL}/link/to/file.tar.gz \
  -it alpine-apache-php /bin/ash
```

Without environment variables (`--entrypoint` prevents it from going into the actual `ENTRYPOINT` defined in the Dockerfile.):
```
docker run -p 9080:80 -it --entrypoint="" alpine-apache-php /bin/ash
```

### With MySQL
Start a MySQL instance:
```
docker run --name mysql -e MYSQL_ROOT_PASSWORD=pass -d percona
```

Start the web instance, linking MySQL to it:
```
docker run \
  --name project_name --link mysql:mysql -d -p 9080:80 \
  -e AWS_ACCESS_KEY_ID={your_aws_access_key} \
  -e AWS_SECRET_ACCESS_KEY={your_aws_secret_access_key} \
  -e DEPLOY_FILE=s3://{BUCKET.URL}/link/to/file.tar.gz \
  -e SITE_ENV={dev|test|prod}
  -e DRUPAL_DB_NAME={a database name}
  -e DRUPAL_DB_USERNAME={db user}
  -e DRUPAL_DB_PASS={db pass}
  -it alpine-apache-php /bin/ash
```

[![](https://images.microbadger.com/badges/image/yusufhm/alpine-apache-php.svg)](https://microbadger.com/images/yusufhm/alpine-apache-php "Get your own image badge on microbadger.com")
