#!/bin/sh

if [ -n "$DEPLOY_FILE" ]; then
  # Get the release.
  s3cmd get $DEPLOY_FILE /tmp/release.tar.gz
  mkdir -p /site
  tar -xf /tmp/release.tar.gz -C /site/
  rm -rf /site/docroot/sites/default/files/
  ln -s /files /site/docroot/sites/default/
  rm /tmp/release.tar.gz

  sed -i 's#DocumentRoot "/var/www/localhost/htdocs"#DocumentRoot "/site/docroot"#g' /etc/apache2/httpd.conf
  sed -i 's#<Directory "/var/www/localhost/htdocs"#<Directory "/site/docroot"#g' /etc/apache2/httpd.conf
  sed -i 's#DirectoryIndex index.html#DirectoryIndex index.php#' /etc/apache2/httpd.conf
  sed -i '/LoadModule rewrite_module/s/^#//' /etc/apache2/httpd.conf
  sed -i '\#^<Directory "/site/docroot">#,\#^</Directory># s#AllowOverride None#AllowOverride all#' /etc/apache2/httpd.conf
fi

httpd -D FOREGROUND
