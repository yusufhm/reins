## Alpine Ansible
Installed from an alpine base, with ansible.

To build, run the following:
```
docker build -t alpine-ansible .
```

### Trying it out
```
docker run \
  -e AWS_ACCESS_KEY_ID={{your_aws_access_key}} \
  -e AWS_SECRET_ACCESS_KEY={{your_aws_secret_access_key}} \
  -it alpine-ansible /bin/ash
```

[![](https://images.microbadger.com/badges/image/yusufhm/alpine-ansible.svg)](https://microbadger.com/images/yusufhm/alpine-ansible "Get your own image badge on microbadger.com")
