#!/bin/sh

trigger_docker_hub_deploy() {
  repo=$1
  files_to_diff=$2
  source_id=$3
  trigger_id=$4

  set -x
  last_image_commit=`scripts/image-last-commit.py $repo`
  diff=`git diff --stat ${last_image_commit} ${BITBUCKET_COMMIT} ${files_to_diff}`
  if [ -n "${diff}" ]; then
    curl -H "Content-Type: application/json" --data '{"build": true}' -X POST https://cloud.docker.com/api/build/v1/source/${source_id}/trigger/${trigger_id}/
  fi
}

# TODO: Update trigger url to new one.
trigger_docker_hub_deploy 'yusufhm/alpine-apache-php' 'dockerfiles/alpine-apache-php' '' 'b8240c6a-ed40-4171-bb48-c5fe1d0b698d'
trigger_docker_hub_deploy 'yusufhm/alpine-blt-builder' 'dockerfiles/alpine-blt-builder' '02d66ea2-3f38-4b09-a022-a1a213807860' 'b8682b04-d05e-41f1-ae6c-8a80c4f5f96d'
# TODO: Update trigger url to new one.
trigger_docker_hub_deploy 'yusufhm/alpine-ansible' 'dockerfiles/alpine-ansible' '' '9858bba0-80f9-4dc4-9623-e1010de4a66d'
# TODO: Update trigger url to new one.
trigger_docker_hub_deploy 'yusufhm/alpine-node-docker' 'dockerfiles/alpine-node-docker' '' 'c9209e8c-f11b-4a5e-b7bc-59af6b7492f4'
# TODO: Update trigger url to new one.
trigger_docker_hub_deploy 'yusufhm/circleci-blt-builder' 'dockerfiles/circleci-blt-builder' '' '0f48a891-95b1-4132-8343-f6217a83e830'
