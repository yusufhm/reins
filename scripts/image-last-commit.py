#!/usr/bin/env python

import sys
import urllib2
import json

def get_token(image_name):
    """Get a token for the image.

    Keyword arguments:
    image_name -- in the form {org}/{repo_name}
    """

    # Get a bearer token.
    token_url = "https://auth.docker.io/token?" + \
        "service=registry.docker.io&" + \
        "scope=repository:" + image_name + ":pull"
    response = urllib2.urlopen(token_url)
    html = response.read()
    j = json.loads(html)

    # Retrieve the token.
    if not "token" in j:
        return None

    return j["token"]

def get_last_image_commit(image_name):
    """Get the last commit for the image, which is stored in the vcs-ref label.

    Keyword arguments:
    image_name -- in the form {org}/{repo_name}
    """

    # Get a token.
    token = get_token(image_name)

    # Use the token to get the image information.
    image_info_url = "https://registry-1.docker.io/v2/" + \
        image_name + "/manifests/latest"
    headers = {
        "Authorization": "Bearer " + token,
        "Docker-Distribution-API-Version": "registry/2.0"
    }
    request = urllib2.Request(image_info_url, headers=headers)
    try:
        response = urllib2.urlopen(request)
        html = response.read()
        j = json.loads(html)

        # The commit is in the history.
        if not "history" in j:
            return None

        image_info = j["history"][0]

        if not "v1Compatibility" in image_info:
            return None

        compat = json.loads(image_info["v1Compatibility"])

        if not "config" in compat:
            return None

        if not "Labels" in compat["config"]:
            return None

        if not "org.label-schema.vcs-ref" in compat["config"]["Labels"]:
            return None

        return compat["config"]["Labels"]["org.label-schema.vcs-ref"]
    except urllib2.HTTPError as e:
        print e.read()
        print e.code
        print e.reason

    return None

def main():
    image_name = sys.argv[1]
    print get_last_image_commit(image_name)

if __name__ == "__main__":
    main()
