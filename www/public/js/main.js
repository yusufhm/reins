(function (window, Request, document) {

    const makeRequest = function (url, elementId) {
        const request = new Request(url);
        fetch(request)
            .then(response => {
                if (response.status === 200) {
                    return response.json();
                } else {
                    throw new Error('Something went wrong on api server!');
                }
            })
            .then(jsonResponse => {
                document.getElementById(elementId).innerHTML = jsonResponse.html;
            })
            .catch(error => {
                console.error(error);
            });
    };

    window.onload = function() {
        const dataWrappers = document.getElementsByClassName('api-data-wrapper');
        for (let dataWrapper of dataWrappers) {
            let url = dataWrapper.getAttribute('data-api-url');
            let wrapperId = dataWrapper.getAttribute('id');
            if (url && wrapperId) {
                makeRequest(url, wrapperId);
            }
        }
    };
})(window, window.Request, document);
