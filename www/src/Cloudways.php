<?php

namespace App;

use GuzzleHttp\Client;

class Cloudways {

  const BASE_URI = 'https://api.cloudways.com/api/v1/';

  private $email;

  private $secret;

  private $client;

  private $access_token;

  public function __construct() {
    $this->email = getEnv('CLOUDWAYS_API_EMAIL');
    $this->secret = getEnv('CLOUDWAYS_API_SECRET');
    $this->client = new Client(['base_uri' => self::BASE_URI]);
  }

  public function getAccessToken() {
    $response = $this->client->request('GET', 'oauth/access_token', [
        'query' => ['email' => $this->email, 'api_key' => $this->secret],
        'headers' => ['Accept' => 'application/json'],
    ]);
    if ($response->getStatusCode() === 200) {
      $body = (string) $response->getBody();
      $json = json_decode($body, TRUE);
      $this->access_token = $json['access_token'];
      return $this->access_token;
    }
  }

  public function getServers() {
    if (!$this->access_token) {
      $this->getAccessToken();
    }
    $response = $this->client->request('GET', 'server', [
        'query' => ['email' => $this->email, 'api_key' => $this->secret],
        'headers' => [
            'Authorization' => 'Bearer ' . $this->access_token,
            'Accept' => 'application/json'
        ],
    ]);
    if ($response->getStatusCode() === 200) {
      $body = (string) $response->getBody();
      $json = json_decode($body, TRUE);
      return $json['servers'];
    }
  }

}
