<?php

namespace App;

use GuzzleHttp\Client;

class CircleCi {

  private $token;

  private $client;

  public function __construct() {
    $this->token = getEnv('CIRCLECI_API_TOKEN');
    $this->client = new Client(['base_uri' => 'https://circleci.com/api/v1.1/']);
  }

  public function getProjects() {
    $response = $this->client->request('GET', 'projects', [
        'query' => ['circle-token' => $this->token],
        'headers' => ['Accept' => 'application/json'],
    ]);
    if ($response->getStatusCode() === 200) {
      $body = (string) $response->getBody();
      $json = json_decode($body, TRUE);
      return $json;
    }
  }

  public function getRecentBuilds() {
    $response = $this->client->request('GET', 'recent-builds', [
        'query' => ['circle-token' => $this->token],
        'headers' => ['Accept' => 'application/json'],
    ]);
    if ($response->getStatusCode() === 200) {
      $body = (string) $response->getBody();
      $json = json_decode($body, TRUE);
      return $json;
    }
  }

}
