<?php

namespace App;

use \TelegramBot\Api\BotApi;

class TelegramBot extends BotApi {

    public function __construct() {
        parent::__construct(getEnv('TELEGRAM_BOT_TOKEN'));
    }

}
