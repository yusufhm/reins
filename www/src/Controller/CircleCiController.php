<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;

use App\CircleCi;

class CircleCiController extends AbstractController {

  /**
   * @Route("/api/circleci/projects", name="circleci_projects")
   */
  public function projects(Request $request, CircleCi $circleci) {
    $projects = $circleci->getProjects();
    $render = $this->render('circleci-projects.html.twig', ['projects' => $projects]);
    return $this->json(['html' => $render->getContent()]);
  }

  /**
   * @Route("/api/circleci/recent-builds", name="circleci_recent_builds")
   */
  public function recentBuilds(Request $request, CircleCi $circleci) {
    $recent_builds = $circleci->getRecentBuilds();
    $render = $this->render('circleci-builds.html.twig', ['builds' => array_slice($recent_builds, 0, 5)]);
    return $this->json(['html' => $render->getContent()]);
  }

}
