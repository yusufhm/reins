<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;

use App\TelegramBot;

class ReinsController extends AbstractController {

    /**
     * @Route("/notify", name="notify")
     */
    public function notify(Request $request, TelegramBot $bot) {
      if ($request->query->get('notifyKey') !== getEnv('NOTIFY_KEY')) {
        return $this->json(['status' => 'Wrong Key']);
      }
      if (!$request->query->get('message')) {
        return $this->json(['status' => 'Message empty']);
      }
      $bot->sendMessage(getEnv('TELEGRAM_USER_ID'), $request->query->get('message'));
      return $this->json(['status' => 'OK']);
    }

}
