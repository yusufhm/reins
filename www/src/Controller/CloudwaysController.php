<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;

use App\Cloudways;

class CloudwaysController extends AbstractController {

  /**
   * @Route("/api/cloudways", name="cloudways")
   */
  public function cloudways(Request $request, Cloudways $cloudways) {
    $servers = $cloudways->getServers();
    $render = $this->render('cloudways.html.twig', ['servers' => $servers]);
    return $this->json(['html' => $render->getContent()]);
  }

}
